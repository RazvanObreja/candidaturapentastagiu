/*2. Given a NxN matrix. 
 * Find the largest product of M consecutive 
 * elements placed on the same line, column or diagonal.   
 * Eg: In the following 5x5 matrix, find the largest 3 factors product:   
	1 1 1 4 1 
	1 2 5 8 1 
	1 9 1 0 2 
	2 3 1 2 5 
	1 1 2 3 1   Expected result: 4 * 5 * 9 = 180*/
	
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
public class Matrix {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan= new Scanner(System.in);
		double[][]matrix ={
				  { 1, 1, 1, 4,1 },
				  { 1, 2, 5, 8, 1 },
				  { 1, 9, 1, 0, 2 },
				  { 2, 3, 1, 2, 5 },
				  { 1, 1, 2, 3, 1 }
				}; 
		
		int number = 0;
		do{
			System.out.print("Introduceti numarul: " );
			number = scan.nextInt();
		}while(number > matrix.length);

		ArrayList<Double> elementsRows = new ArrayList<Double>();
		ArrayList<Double> elementsColumns = new ArrayList<Double>();
		ArrayList<Double> finalArrayList = new ArrayList<Double>();
		double diagonalMultiplication = 0;
		double maxRow = 0;
		double maxColumn = 0;
		double finalResult = 0;
		
		
		double multiplication = 0;
		double columnsMultiplication = 0;
		
		for(int row = 0; row <= number-1; row++)
		{
			multiplication = 1;
			columnsMultiplication = 1;
		    for(int column = 0; column <= number-1; column++)
		    {
		    	multiplication = multiplication*matrix[row][column];
		    	columnsMultiplication = columnsMultiplication*matrix[column][row];
		    }
		    elementsRows.add(multiplication);
		    elementsColumns.add(columnsMultiplication);
		}
		maxRow = Collections.max(elementsRows);
		maxColumn = Collections.max(elementsColumns);
		diagonalMultiplication = diagonalMultiplication(matrix, number);
		finalArrayList.add(diagonalMultiplication);
		finalArrayList.add(maxRow);
		finalArrayList.add(maxColumn);
		
		finalResult = Collections.max(finalArrayList);

		System.out.println("Produsul cel mai mare a "+number+" elemente consecutive din matrice este: "+finalResult);
	}
	
	public static double diagonalMultiplication(double[][]matrix, int number)
	{
		double multiplication = 1;
		for (int diag=0; diag<number; diag++)
		{
			multiplication = multiplication*matrix[diag][diag];
		}
		return multiplication;
	}

}