package pkg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) {
		int dimension = 0;
		System.out.println("Give the length of the series!");
		System.out.println("n! = ");

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			dimension = Integer.parseInt(br.readLine());
		} catch (NumberFormatException ne) {
			System.out.println("Invalid format: " + ne);
			System.exit(0);
		} catch (IOException ioe) {
			System.out.println("IO exception: " + ioe);
			System.exit(0);
		}

		Methods.rezFinal(dimension);

	}
}
