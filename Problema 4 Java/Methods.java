package pkg;

public class Methods {

	// method for calculating the factorial
	// n! means n � (n - 1) � ... � 3 � 2 � 1
	private  double factorial(int n) 
	{
		if (n == 0)
			return 1;
		else
			return (n * factorial(n - 1));
	}

	/*
	 * the method for calculating the sum of the digits in the number Example:
	 * 8! = 40320 9! = 362880 10! = 10 � 9 � ... � 3 � 2 � 1 = 3628800, The sum
	 * of the digits in the numbers are: digitSum(8!) = 4 + 0 + 3 + 2 + 0 = 9
	 * digitSum(9!) = 3 + 6 + 2 + 8 + 8 + 0 = 27 digitSum(10!) = 3 + 6 + 2 + 8 +
	 * 8 + 0 + 0 = 27
	 */
	private  int digitSum(double n)
	{
		int sum = 0;
		while (n != 0) 
		{
			sum += (n % 10);
			n = n / 10;
		}
		return sum;
	}

	/*
	 * Given a series of numbers 1!..n! find and print the longest sequence of
	 * numbers where the digit sum for their factorials does not increase.
	 * digitSum(x!) <= digitSum((x+1)!) .. <= digitSum((x+k))
	 */

	public void rezFinal(int n) 
	{
		// position n in the array coincide with sum of the digits for n!
		int[] vect = new int[n + 2]; // array of integers with sum of the digits
		int index = 0;// index for the beginning of the largest sequence of numbers
		int dimension = 0;// size of the largest sequence of numbers
		int aux_dimension = 0;// the size of each sequence
		int i, j;

		for (i = 1; i <= n; i++) 
		{
			vect[i] = digitSum(factorial(i));
		}
		
		for (i = 1; i <= n; i++)
			{
			for (j = i; j <= n; j++)
				{
				if (vect[j] <= vect[j + 1])
					{
					aux_dimension++;
				} else
					{
					if (dimension < aux_dimension)
						{
						dimension = aux_dimension;
						index = i;
					}
					aux_dimension = 0;
					i = j;
					break;
				}
			}
		}
		System.out.println("Sequence is :");
		for (i = index; i <= index + dimension; i++)
			{
			System.out.print(i + " ");
		}				
	}

}
